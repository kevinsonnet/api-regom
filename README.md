# Echange de donnée avec l'ERP

## Services Web

### Clients

Retourne la liste complete des clients
```apache
GET /customers
```

Retourne la liste des clients ayant été modifiés depuis t (timestamp en seconde)
```apache
GET /customers?t=1620109092
```

Retourne le client ayant pour identifiant {id}
```apache
GET /customers/{id}
```

Retour json attendu, data sera un array pour /customers, et un objet directement pour /customers/{id}
```json
{
	"data": {
		"id_customer": 1,
		"firstname": "Michel",
		"lastname": "Dupont",
		"company": "Ma société",
		"email": "michel.dupont@gmail.com",
		"address": {
			"firstname": "Michel",
			"lastname": "Dupont",
			"company": "Ma société",
			"phone": "0600000000",
			"phone2": "",
			"address": "1 rue de la republique",
			"address2": "",
			"other": "",
			"postcode": 21000,
			"city": "Dijon",
			"country_iso_code": "FR",
			"[AUTRE]": "..."
		},
		"[AUTRE]": "..."
	}
}
```

A noter que l'email est obligatoire car il servira d'identifiant

Séparation du client et de l'adresse ? Possibilité d'avoir plusieurs adresses ?

Envoi les informations des client en bulk pour création/mise à jour (définir la clé commune)
```apache
POST /customers
```

### Produits

Retourne la liste complete des produits
```apache
GET /products
```

Retourne la liste des produits ayant été modifiés depuis t (timestamp en seconde)
```apache 
GET /products?t=1620109092
```

Retourne le produit ayant pour identifiant {id}
```apache
GET /products/{id}
```

Retour json attendu, data sera un array pour /products, et un objet directement pour /products/{id}
```json
{
	"data": [
		{
			"id_product": 1,
			"name": "Nom du produit",
			"reference": "Ma ref",
			"price": 45.10,
			"tax_rate": 20,
			"quantity": 50,
			"image": "nom ou url de l'image",
			"category": "Ma catégorie",
			"[AUTRE]": "..."
		}
	]
}
```

Categories à définir, soit identification par nom directement via le flux, soit un flux /categories avec les identifiants et utilisation des identifiants dans le flux produits

Si impossbilité de faire de l'incrémentiel avec le paramètre t, il faudra alors ajouter une route pour le stock
```apache
GET /products/stock
```

Route pour un produit
```apache
GET /products/{id}/stock
```

```json
{
	"data": [
		{
			"id_product": 1,
			"quantity": 5,
		}
	]
}
```

Pour les images nous pouvons soit:
- Envoyer un nom d'image dans le flux, déposer les images sur un ftp et aller chercher les images sur le ftp via ce nom
- Fournir une url de l'image dans le flux

### Commandes

```apache
POST /orders
```

```json
{
	"data": [
		{
			"id_order": 1,
			"id_customer": 1,
			"invoice_address": {
				"firstname": "Michel",
				"lastname": "Dupont",
				"company": "Ma société",
				"phone": "0600000000",
				"phone2": "",
				"address": "1 rue de la republique",
				"address2": "",
				"other": "",
				"postcode": 21000,
				"city": "Dijon",
				"country_iso_code": "FR"
			},
			"delivery_address": {
				"firstname": "Michel",
				"lastname": "Dupont",
				"company": "Ma société",
				"phone": "0600000000",
				"phone2": "",
				"address": "1 rue de la republique",
				"address2": "",
				"other": "",
				"postcode": 21000,
				"city": "Dijon",
				"country_iso_code": "FR"
			}
			"order_details": [
				{
					"id_product": 1,
					"name": "Nom du produit",
					"reference": "123",
					"price": 54.10,
					"tax_rate": 20,
					"quantity": 1,
					"[AUTRE]": "..."
				}
			],
			"[AUTRE]": "..."
		}
	]
}
```

## Informations techniques

Quelle dispo pour le serveur ?  
Quand faire les appels ?  
Fréquence des appels ?  

Si Service Web trop complexe, possibilité d'utiliser un FTP et des csv
